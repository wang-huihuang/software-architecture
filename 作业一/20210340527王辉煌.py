# 获取第三方平台数据：获取指定城市现在的天气情况
# 天气预报接口：juhe.cn

import requests
import json

def output(data):
    try:
        tq = data["result"]["realtime"]
        print(f'{data["result"]["city"]}当前的天气情况：')
        print(f"温度：{tq['temperature']}°C")
        print(f"湿度：{tq['humidity']}")
        print(f"天气情况：{tq['info']}")
        print(f"风向风力：{tq['direct']}{tq['power']}")
        print(f"空气质量指数：{tq['aqi']}")
    except Exception as e:
        print("解析结果异常：%s" % e)


if __name__  ==  "__main__":
    headers = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36"}

    key = "99d1e4f4309813bf881c75f0706a390f"
    cityName = input("请输入想要查询的城市：")

    param = {"city":cityName,"key":"4080fdf35495a31a75cab22449d0b5ac"}
    url = f"http://apis.juhe.cn/simpleWeather/query"
    response = requests.get(url, headers=headers, params=param)

    data = response.json()
    # print(data,type(data))
    output(data)

# 数据解析
# with open("./json_files/tianqi.json","r",encoding="utf8") as f:
#     data = json.load(f)
#     print(f'{data["result"]["city"]}当前的天气情况：')
#     tq = data["result"]["realtime"]
#     print(f"温度：{tq['temperature']}°C")
#     print(f"湿度：{tq['humidity']}")
#     print(f"天气情况：{tq['info']}")
#     print(f"风向：{tq['direct']}")
#     print(f"风力：{tq['power']}")
#     print(f"空气质量指数：{tq['aqi']}")






